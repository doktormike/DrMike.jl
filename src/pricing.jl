"""
    saasprice(price)

Convert the Software as a Service (SaaS) price per month into a cognitively
preferentiable price.
"""
function saasprice(price)
    if price < 200
        r = mod(price, 10)
        if r > 4
            return price + 10 - r - 1
        else
            return price - r - 1
        end
    else
        r = mod(price, 50)
        return price + (50 - r)
    end
end
