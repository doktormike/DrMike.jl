# DrMike

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://DoktorMike.gitlab.io/DrMike.jl/dev)
[![Build Status](https://gitlab.com/DoktorMike/DrMike.jl/badges/main/pipeline.svg)](https://gitlab.com/DoktorMike/DrMike.jl/pipelines)
[![Coverage](https://gitlab.com/DoktorMike/DrMike.jl/badges/main/coverage.svg)](https://gitlab.com/DoktorMike/DrMike.jl/commits/main)
