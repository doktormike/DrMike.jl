```@meta
CurrentModule = DrMike
```

# DrMike

Documentation for [DrMike](https://gitlab.com/DoktorMike/DrMike.jl).

```@index
```

```@autodocs
Modules = [DrMike]
```
