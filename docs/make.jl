using DrMike
using Documenter

DocMeta.setdocmeta!(DrMike, :DocTestSetup, :(using DrMike); recursive=true)

makedocs(;
    modules=[DrMike],
    authors="Michael Green <micke.green@gmail.com> and contributors",
    sitename="DrMike.jl",
    repo="gitlab.com/doktormike/DrMike.jl.git",
    format=Documenter.HTML(;
        canonical="https://DoktorMike.gitlab.io/DrMike.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
